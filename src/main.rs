extern crate getopts;
use getopts::{Options, Matches};
use std::env;

use std::net::{TcpListener, TcpStream};
use std::net::SocketAddr;

use std::thread;

use std::str::FromStr;
use std::io::Read;
use std::io::Write;

mod bencode;

fn opt_default(matches: &Matches, name: &str, default: &str) -> String {
    match matches.opt_str(name) {
        Some(opt) => opt,
        None => default.to_string()
    }
}

fn handle(mut stream: TcpStream) {
    println!("Connected");
    loop {
        let mut buf = [0; 512];
        match stream.read(&mut buf) {
            Err(e) => panic!(e.to_string()),
            Ok(length) if length == 0 => break,
            _ => {}
        }

        match stream.write(&buf) {
            Err(e) => panic!(e.to_string()),
            Ok(_) => {}
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut opts = Options::new();
    opts.optopt("i", "ip", "set the ip address of the listening socket", "IP");
    opts.optopt("p", "port", "set the port of the listening socket", "PORT");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(e) => panic!(e.to_string())
    };

    let ip = opt_default(&matches, "ip", "127.0.0.1");
    let port = opt_default(&matches, "port", "8888");
    let addr = match SocketAddr::from_str(format!("{}:{}", ip, port).as_str()) {
        Ok(addr) => addr,
        Err(e) => panic!(e.to_string())
    };

    println!("Listening");
    let listener = TcpListener::bind(addr).unwrap();
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => { thread::spawn(move || {handle(stream)}); },
            Err(e) => { println!("Connection failed: {}", e); }
        };
    }
}
