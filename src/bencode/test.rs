
use std::collections::BTreeMap;

use super::Bencode;
use super::Bencode::{Integer, ByteString, List, Dict};

#[test]
fn positive_encode_num() {
    assert_eq!(String::from("i10e"), Integer(10).encode());
}

#[test]
fn negative_encode_num() {
    assert_eq!(String::from("i-10e"), Integer(-10).encode());
}

#[test]
fn zero_encode_num() {
    assert_eq!(String::from("i0e"), Integer(0).encode());
}

#[test]
fn non_empty_encode_word() {
    assert_eq!(String::from("4:word"), ByteString(String::from("word")).encode());
}

#[test]
fn empty_encode_word() {
    assert_eq!(String::from("0:"), ByteString(String::from("")).encode());
}

#[test]
fn one_encode_list() {
    let nodes = vec![ByteString(String::from("test"))];
    assert_eq!(String::from("l4:teste"), List(nodes).encode());
}

#[test]
fn many_encode_list() {
    let nodes = vec![ByteString(String::from("word")), Integer(10)];
    assert_eq!(String::from("l4:wordi10ee"), List(nodes).encode());
}

#[test]
fn empty_encode_list() {
    assert_eq!(String::from("le"), List(vec![]).encode());
}

#[test]
fn one_encode_dict() {
    let mut entries = BTreeMap::new();
    let val = ByteString(String::from("value"));
    entries.insert(String::from("key"), val);
    assert_eq!(String::from("d3:key5:valuee"), Dict(entries).encode());
}

#[test]
fn empty_encode_dict() {
    assert_eq!(String::from("de"), Dict(BTreeMap::new()).encode());
}

#[test]
fn positive_decode_num() {
    let expected = Integer(10);
    let actual = Bencode::decode(&mut String::from("i10e")).unwrap();
    assert_eq!(expected, actual);
}

#[test]
fn negative_decode_num() {
    let expected = Integer(-10);
    let actual = Bencode::decode(&mut String::from("i-10e")).unwrap();
    assert_eq!(expected, actual);
}

#[test]
fn zero_decode_num() {
    let expected = Integer(0);
    let actual = Bencode::decode(&mut String::from("i0e")).unwrap();
    assert_eq!(expected, actual);
}

#[test]
fn non_empty_decode_word() {
    let expected = ByteString(String::from("test"));
    let actual = Bencode::decode(&mut String::from("4:test")).unwrap();
    assert_eq!(expected, actual);
}

#[test]
fn empty_decode_word() {
    assert!(Bencode::decode(&mut String::from("")).is_err());
}

#[test]
fn one_decode_list() {
    let nodes = vec![Integer(5)];
    let expected = List(nodes);
    let actual = Bencode::decode(&mut String::from("li5ee")).unwrap();
    assert_eq!(expected, actual);
}

#[test]
fn many_decode_list() {
    let nodes = vec![
        ByteString(String::from("test")),
        ByteString(String::from("one")),
        ByteString(String::from("two")),
        ByteString(String::from("three")),
    ];
    let expected = List(nodes);
    let actual = Bencode::decode(&mut String::from("l4:test3:one3:two5:threee")).unwrap();
    assert_eq!(expected, actual);

}

#[test]
fn empty_decode_list() {
    let expected = List(vec![]);
    let actual = Bencode::decode(&mut String::from("le")).unwrap();
    assert_eq!(expected, actual);
}

#[test]
fn one_decode_dict() {
    let mut entries = BTreeMap::new();
    entries.insert(String::from("test"), Integer(5));
    let expected = Dict(entries);
    let actual = Bencode::decode(&mut String::from("d4:testi5ee")).unwrap();
    assert_eq!(expected, actual);
}

#[test]
fn many_decode_dict() {
    let mut entries = BTreeMap::new();
    entries.insert(String::from("key1"), ByteString(String::from("one")));
    entries.insert(String::from("key2"), ByteString(String::from("two")));
    entries.insert(String::from("key3"), ByteString(String::from("three")));
    let expected = Dict(entries);
    let actual = Bencode::decode(&mut String::from("d4:key13:one4:key23:two4:key35:threee")).unwrap();
    assert_eq!(expected, actual);

}

#[test]
fn empty_decode_dict() {
    let expected = Dict(BTreeMap::new());
    let actual = Bencode::decode(&mut String::from("de")).unwrap();
    assert_eq!(expected, actual);
}
