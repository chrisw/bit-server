use std::fmt;

use std::collections::BTreeMap;
use std::error;
use std::iter::Peekable;
use std::num::ParseIntError;

#[derive(Debug)]
pub enum Error {
    NumParsingError,
    WordParsingError,
    ListParsingError,
    DictParsingError,
    UnexpectedByteError,
    UnexpectedEndError,
}

impl error::Error for Error {
    fn description(&self) -> &str {
        "Encountered an error while parsing your byte string"
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Encountered an error while parsing your byte string")
    }
}

#[derive(Debug, PartialEq)]
pub enum Bencode {
    Integer(i64),
    ByteString(String),
    List(Vec<Bencode>),
    Dict(BTreeMap<String, Bencode>),
}

impl Bencode {
    pub fn encode(&self) -> String {
        encode_bnode(self)
    }

    pub fn decode(byte_string: &String) -> Result<Bencode, Error> {
        decode_bytes(&mut byte_string.chars().peekable())
    }
}

fn encode_bnode(bnode: &Bencode) -> String {
    use self::Bencode::*;
    match *bnode {
        Integer(num) => encode_num(&num),
        ByteString(ref word) => encode_word(&word),
        List(ref nodes) => encode_list(&nodes),
        Dict(ref entries) => encode_dict(&entries),
    }
}

fn encode_num(num: &i64) -> String {
    format!("i{}e", num)
}

// TODO: Should you be able to encode the empty string? Probably not.
fn encode_word(word: &String) -> String {
    format!("{}:{}", word.len(), word)
}

fn encode_list(nodes: &Vec<Bencode>) -> String {
    let contents = nodes.iter().map(encode_bnode).collect::<String>();
    format!("l{}e", contents)
}

fn encode_dict(entries: &BTreeMap<String, Bencode>) -> String {
    let contents = entries.iter()
        .map(|(key, val)| {
            format!("{}{}", encode_word(&key), encode_bnode(&val))
        }).collect::<String>();
    format!("d{}e", contents)
}

fn decode_bytes<I>(mut chars: &mut Peekable<I>) -> Result<Bencode, Error>
where I: Iterator<Item = char> {
    if let Some(peek_char) = chars.peek().map(|&c| c) {
        match peek_char {
            'i' => decode_num::<I>(&mut chars),
            'l' => decode_list::<I>(&mut chars),
            'd' => decode_dict::<I>(&mut chars),
            '0' ... '9' => decode_word::<I>(&mut chars),
            _ => Err(Error::UnexpectedByteError)
        }
    } else {
        Err(Error::UnexpectedEndError)
    }
}

fn decode_num<I>(mut chars: &mut Peekable<I>) -> Result<Bencode, Error>
where I: Iterator<Item= char> {
    assert_eq!(Some('i'), chars.next());
    match chars_to_num(&mut chars, 'e') {
        Ok(num) => Ok(Bencode::Integer(num)),
        Err(_) => Err(Error::NumParsingError)
    }
}

fn decode_word<I>(mut chars: &mut Peekable<I>) -> Result<Bencode, Error>
where I: Iterator<Item= char> {
    match chars_to_num(&mut chars, ':') {
        Ok(length) => {
            let word = chars.take(length as usize).collect::<String>();
            Ok(Bencode::ByteString(word))
        },
        Err(_) => Err(Error::WordParsingError)
    }
}

fn decode_list<I>(mut chars: &mut Peekable<I>) -> Result<Bencode, Error>
where I: Iterator<Item= char> {
    assert_eq!(Some('l'), chars.next());
    let mut nodes = Vec::new();
    loop {
        if chars.peek().map(|&c| c) == Some('e') {
            return Ok(Bencode::List(nodes))
        }
        let bnode = decode_bytes::<I>(&mut chars)?;
        nodes.push(bnode);
    };
}

fn decode_dict<I>(mut chars: &mut Peekable<I>) -> Result<Bencode, Error>
where I: Iterator<Item= char> {
    assert_eq!(Some('d'), chars.next());
    let mut entries = BTreeMap::new();
    loop {
        if chars.peek().map(|&c| c) == Some('e') {
            return Ok(Bencode::Dict(entries))
        }
        if let Bencode::ByteString(word) = decode_bytes::<I>(&mut chars)? {
            let bnode = decode_bytes::<I>(&mut chars)?;
            entries.insert(word, bnode);
        } else {
            return Err(Error::DictParsingError)
        }
    };
}

fn chars_to_num<I>(chars: &mut Peekable<I>, terminator: char)
    -> Result<i64, ParseIntError>
    where I: Iterator<Item= char> {
        chars.take_while(|&c| { c != terminator })
            .collect::<String>()
            .parse::<i64>()
    }

#[cfg(test)]
mod test;
